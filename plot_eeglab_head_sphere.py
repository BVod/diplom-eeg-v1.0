import mne
import numpy as np
import matplotlib.pyplot as plt


biosemi_montage = mne.channels.make_standard_montage(kind='standard_1020', head_size=0.095)
ch_names = ['C4', 'C3', 'Cz', 'F3', 'F4', 'F8', 'F7', 'Fp1', 'Fp2', 'Fz', 'O1', 'O2', 'P3', 'P4', 'Pz', 'T3', 'T4', 'T5', 'T6']
n_channels = len(ch_names)
fake_info = mne.create_info(ch_names=ch_names, sfreq=500,
                            ch_types='eeg')
rng = np.random.RandomState(0)
data = rng.normal(size=(n_channels, 1)) * 1e-6
#data = np.array([[0],[10],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0]])
fake_evoked = mne.EvokedArray(data, fake_info)
fake_evoked.set_montage(biosemi_montage)
fig, ax = plt.subplots(ncols=2, figsize=(8, 4), gridspec_kw=dict(top=0.9),
                       sharex=True,
                       sharey=True)
fake_evoked.plot_sensors(axes=ax[0], show_names=True, show=True)
fig.texts[0].remove()
mne.viz.plot_topomap(fake_evoked.data[:, 0], fake_evoked.info,
                     names=ch_names,
                     show_names=True,
                     show=True)
plt.show()