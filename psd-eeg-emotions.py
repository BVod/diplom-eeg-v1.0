import numpy as np
import os
import pyedflib
from multiprocessing import Pool, cpu_count
import pandas as pd
import json
from scipy import signal
from scipy.signal import welch
# import dfa_wrapper as dfa_wr
import matplotlib.pyplot as plt
from viz_one_head import viz_one_head
from matplotlib import cm

config = 0


def get_freq_bands_rel_power(X, freq, ch_names, freq_bands=None, method="trapez"):
    """
    :param X: array_like 2-D, 2D Matrix of PSD (each row corresponds to EEG channel) to be sliced
    :param freq: 1D array of corresponding frequencies
    :param ch_names: [list] list of EEG channel names
    :param freq_bands: dict of lists, where Keys are band names and Values are list in form [<upper bound>, <lower bound>]
    :param method: [str], [sims/trapez]
            If "sims", integrate using samples along the given axis and the composite Simpson’s rule.
            If "trapez", integrate along the given axis using the composite trapezoidal rule.
    :return: [pandas DataFrame], Sliced bands aggregated by pre-defined functions
    """
    from scipy.integrate import simps
    from numpy import trapz

    if method == "simps":
        integration_method = simps
    elif method == "trapez":
        integration_method = trapz
    else:
        raise ValueError(f"method={method} is not defined. Please, pass method = [simps/trapez]!")

    assert (len(ch_names) == X.shape[0])

    # Frequency resolution
    freq_res = freq[1] - freq[0]
    # Compute total power
    sum_ampl = integration_method(X, dx=freq_res)
    # Normalize each frequency bean on total power (convert each frequency to a percentage of total power)
    # norm_psd = X / sum_ampl.reshape(-1, 1)

    df = pd.DataFrame()
    for key, (low, high) in freq_bands.items():
        # Find indexes of subband
        idx_band = np.logical_and(freq >= low, freq <= high)
        # Compute the relative power of normalized subband
        sub_band = integration_method(X[:, idx_band], dx=freq_res)

        # sub_band_original = integration_method(X[:, idx_band], dx=freq_res)
        norm_subband = sub_band / sum_ampl

        # norm_subband_new = integration_method(norm_psd_new[:, idx_band], dx=freq_res)

        df_dict = {'FREQ_BOUND_NAME': [key] * len(ch_names),
                   'CHANNEL': ch_names,
                   'SUM_NORM': norm_subband
                   }
        df = df.append(pd.DataFrame.from_dict(df_dict, orient='columns'))

    return df


def get_data_from_edf(filepath):
    f = pyedflib.EdfReader(filepath)
    NumberOfSignals = f.signals_in_file
    BirthDate = f.birthdate
    Gender = f.gender
    Name = f.patientname
    SignalLabels = f.getSignalLabels()
    NumberOfSamp = f.getNSamples()[0]
    SignalList = np.zeros((NumberOfSignals, f.getNSamples()[0]))
    Fs = round(len(SignalList[3, :]) / f.file_duration)

    for i in np.arange(NumberOfSignals):
        SignalList[i, :] = f.readSignal(i)

    return SignalList, Fs, SignalLabels, NumberOfSignals, BirthDate, Gender.decode("utf-8"), Name.decode("utf-8")


def generate_data(file):
    # sums = 0
    # for f in file:
    #     sums += f
    #
    # return sums, [1 ,1, 1]
    global config
    file_path, patient_name, exp_id, exp_label, gender = file
    print(file_path, patient_name, exp_id, exp_label)

    # dfa_df = pd.DataFrame()
    psd_df = pd.DataFrame()

    # Read data from file
    signal_mtrx, fs, signal_labels, signals_num, birth_date, _, name = get_data_from_edf(file_path)
    print(f"{name}; exp_id:{exp_id};  exp_label:{exp_label} is being processed...")

    # Exclude unwanted signals and labels
    channel_ids_to_exclude = [idx for idx, element in enumerate(signal_labels) if
                              element in config.get("channels_to_exclude")]
    signal_mtrx = np.delete(signal_mtrx, channel_ids_to_exclude, axis=0)
    signal_labels = np.delete(signal_labels, channel_ids_to_exclude, axis=0)

    # Trim current signals
    start_p = int(10*fs)
    end_p = signal_mtrx.shape[1] - (10 * fs)
    signal_mtrx = signal_mtrx[:, start_p:end_p]

    # Get PSD using Welch method
    f_den, Pxx_den = welch(signal_mtrx, fs, nperseg=10 * fs, window="hamming")
    # Compute relative power of needed frequency bands
    psd_ampl_f_bands = get_freq_bands_rel_power(Pxx_den, freq=f_den, ch_names=signal_labels,
                                                freq_bands=config.get("freq_bands"))  # Slice PDS coefficients by bands
    psd_ampl_f_bands['FILENAME'] = patient_name
    psd_ampl_f_bands['GENDER'] = gender
    psd_ampl_f_bands['EXP_ID'] = exp_id
    psd_ampl_f_bands['EXP_LABEL'] = exp_label
    psd_df = psd_df.append(psd_ampl_f_bands, ignore_index=True)
    psd_df.to_pickle(f"{config.get('output_dir')}{patient_name}.{exp_id}.psd.pkl")
    psd_df.to_csv(f"{config.get('output_dir')}{patient_name}.{exp_id}.psd.csv")

    # dfa_f_bands = get_freq_bands_alpha_exponent(signal_mtrx=signal_mtrx, fs=fs, ch_names=signal_labels,
    #                                             freq_bands=config.get("freq_bands"))
    # dfa_f_bands['FILENAME'] = patient_name
    # dfa_f_bands['GENDER'] = gender
    # dfa_f_bands['EXP_ID'] = exp_id
    # dfa_f_bands['EXP_LABEL'] = exp_label
    # dfa_f_bands['EXP_QUALITY'] = exp_quality
    # dfa_df = dfa_df.append(dfa_f_bands, ignore_index=True)
    # dfa_df.to_pickle(f"output/dfa/{name}.{exp_id}.dfa.pkl")

    print("Done")
    # return psd_df, dfa_df
    return psd_df


def main_data_generation():
    print("Emotional Faces Experiment")

    """---------Setup params--------------"""
    global config
    config = json.loads(open("config.json").read())
    multiprocessing = config.get("multiprocessing")
    data_dir_path = config.get("data_dir")
    output_dir_path = config.get("output_dir")
    exp_id_to_exp_label = config.get("exp_id_to_exp_label")
    """-----------------------------------"""

    if not os.path.exists(data_dir_path):
        os.makedirs(data_dir_path)
    if not os.path.exists(output_dir_path):
        os.makedirs(output_dir_path)

    # Get the list of all files in directory tree at given path
    files = []
    for (dirpath, dirnames, filenames) in os.walk(data_dir_path):
        files += [os.path.join(dirpath, file) for file in filenames if file.endswith('.edf')]

    patients = [f.split("/")[-2].split("_")[0] for f in files]
    exp_ids = [f.split("/")[-1].split("_")[2].split(".")[0] for f in files]
    exp_labels = [exp_id_to_exp_label.get(exp_id) for exp_id in exp_ids]
    gender = [f.split("/")[-1].split("_")[1] for f in files]
    data_list = [X for X in zip(files, patients, exp_ids, exp_labels, gender)]

    if not multiprocessing.get("enable") is not None or multiprocessing.get("enable") is not 1:
        generate_data(data_list[0])
        generate_data(data_list[1])
    else:
        cpu_num = multiprocessing.get("cpu_num")
        p = Pool(cpu_num if (cpu_num is not None) or (cpu_num is not 0) else cpu_count() - 2)
        results = p.map(generate_data, data_list)
        p.close()

        psd_all_df = pd.DataFrame()
        # dfa_all_df = pd.DataFrame()
        # for (psd_tmp_df, dfa_tmp_df) in results:
        for psd_tmp_df in results:
            psd_all_df = psd_all_df.append(psd_tmp_df, ignore_index=True)
            # dfa_all_df = dfa_all_df.append(dfa_tmp_df, ignore_index=True)

        psd_all_df.to_pickle(f"{output_dir_path}psd_all.pkl")
        # dfa_all_df.to_pickle(f"output/dfa_all.pkl")

        psd_all_df.to_csv(f"{output_dir_path}psd_all.csv", index=False)
        # dfa_all_df.to_csv(f"output/dfa_all.csv", index=False)
        print("DOne")


def psd_visualization():
    psd_df = pd.read_csv("output/psd_all.csv")
    config = json.loads(open("config.json").read())

    # get experiment DataFrame
    for exp_id in config.get("exp_id_to_exp_label").keys():
        exp_df = psd_df.loc[psd_df['EXP_ID'] == exp_id]

        # create figure
        fig, ax = plt.subplots(nrows=1, ncols=len(config.get("freq_bands").keys()), figsize=(25, 3))
        fig.suptitle(f"{exp_id} - {config.get('exp_id_to_exp_label').get(exp_id)}")

        vmin_global = np.min(
            [exp_df.loc[exp_df['CHANNEL'] == ch]['SUM_NORM'].mean() for ch in config.get("channels_to_use")])
        vmax_global = np.max(
            [exp_df.loc[exp_df['CHANNEL'] == ch]['SUM_NORM'].mean() for ch in config.get("channels_to_use")])

        # vmin_global = np.min([np.min([exp_df[exp_df['FREQ_BOUND_NAME'] == rhythm][['CHANNEL'] == ch]['SUM_NORM'].mean() for ch in config.get("channels_to_use")]) for rhythm in config.get("freq_bands").keys()])
        # vmax_global = np.max([np.max([exp_df[exp_df['FREQ_BOUND_NAME'] == rhythm][['CHANNEL'] == ch]['SUM_NORM'].mean() for ch in config.get("channels_to_use")]) for rhythm in config.get("freq_bands").keys()])

        vmin_global = np.min([np.min(
            [exp_df[(exp_df['FREQ_BOUND_NAME'] == rhythm) & (exp_df['CHANNEL'] == ch)]['SUM_NORM'].mean() for ch in
             config.get("channels_to_use")]) for rhythm in config.get("freq_bands").keys()])
        vmax_global = np.max([np.max(
            [exp_df[(exp_df['FREQ_BOUND_NAME'] == rhythm) & (exp_df['CHANNEL'] == ch)]['SUM_NORM'].mean() for ch in
             config.get("channels_to_use")]) for rhythm in config.get("freq_bands").keys()])

        # vmax = exp_df['SUM_NORM'].max()

        # get rhythm DataFrame
        tmp_df = pd.DataFrame()
        for i, rhythm in enumerate(config.get("freq_bands").keys()):
            rhythm_df = exp_df.loc[exp_df['FREQ_BOUND_NAME'] == rhythm]

            # get electrode data
            data = {}
            for ch in config.get("channels_to_use"):
                electrode_df = rhythm_df.loc[rhythm_df['CHANNEL'] == ch]
                data.update({ch.split(" ")[1]: np.mean(electrode_df['SUM_NORM'])})

            ch_vals = np.array(list(data.values()))
            im = viz_one_head(map=data, axes=ax[i], vmin=vmin_global, vmax=vmax_global, title=f"{rhythm}")

        # add colorbar
        fig.subplots_adjust(right=0.8, top=0.9)
        cbar_ax = fig.add_axes([0.85, 0.1, 0.01, 0.8])
        norm = cm.colors.Normalize(vmin=vmin_global * 100, vmax=vmax_global * 100)
        fig.colorbar(cm.ScalarMappable(norm=norm, cmap='RdBu_r'), cax=cbar_ax, shrink=0.6)

        plt.savefig(f"output/plots/psd/{exp_id}.png", dpi=200)
        # plt.show()


def test_band_power():
    def create_dummy_data(ch_440=None, ch_220=None):

        amplitude = 0.5  # range [0.0, 1.0]
        fs = 1000  # sampling rate, Hz, must be integer
        duration = 60.0  # in seconds, may be float
        f1 = 440.0  # sine frequency, Hz, may be float
        f2 = 220.0  # sine frequency, Hz, may be float

        # generate samples, note conversion to float32 array
        samples_440 = (amplitude * np.sin(2 * np.pi * np.arange(fs * duration) * f1 / fs)).astype(np.float32)
        samples_220 = (amplitude * np.sin(2 * np.pi * np.arange(fs * duration) * f2 / fs)).astype(np.float32)
        samples_merged = samples_440 + samples_220
        samples_concatenated = np.concatenate([samples_440, samples_220])

        signal_mtrx = []
        for ch in config.get("channels_to_use"):
            if ch == ch_440:
                signal_mtrx.append(samples_440)
            elif ch == ch_220:
                signal_mtrx.append(samples_220)
            else:
                signal_mtrx.append(samples_merged)
        return np.array(signal_mtrx)

    config = json.loads(open("config.json").read())

    freq_bands = {
        '440': [439, 441],
        '220': [219, 221]
    }

    vmin_global = 0
    vmax_global = 1

    # create figure
    fig, ax = plt.subplots(nrows=1, ncols=len(freq_bands.keys()), figsize=(25, 3))


    signal_mtrx = create_dummy_data(ch_440="EEG C3", ch_220="EEG O2")
    # Get PSD using Welch method
    f_den, Pxx_den = welch(signal_mtrx, 1000, nperseg=10 * 1000, window="hamming")
    # Compute relative power of needed frequency bands
    psd_ampl_f_bands = get_freq_bands_rel_power(Pxx_den, freq=f_den, ch_names=config.get("channels_to_use"),
                                                    freq_bands=freq_bands)

    for i, rhythm in enumerate(freq_bands.keys()):
        rhythm_df = psd_ampl_f_bands.loc[psd_ampl_f_bands['FREQ_BOUND_NAME'] == rhythm]

        data = {}
        for ch in config.get("channels_to_use"):
            data.update({ch.split(" ")[1] : float(rhythm_df.loc[rhythm_df["CHANNEL"] == ch]["SUM_NORM"])})

        im = viz_one_head(map=data, axes=ax[i], vmin=vmin_global, vmax=vmax_global, title=f"{rhythm}")

    # add colorbar
    fig.subplots_adjust(right=0.8, top=0.9)
    cbar_ax = fig.add_axes([0.85, 0.1, 0.01, 0.8])
    norm = cm.colors.Normalize(vmin=vmin_global, vmax=vmax_global)
    fig.colorbar(cm.ScalarMappable(norm=norm, cmap='RdBu_r'), cax=cbar_ax, shrink=0.6)
    plt.show()


if __name__ == '__main__':
    main_data_generation()
    # psd_visualization()
    # test_band_power()
