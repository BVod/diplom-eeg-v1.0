import mne
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def viz_head_eeg(data_frame, keys_rhythms, name_of_signal, vmin=None, vmax=None):
    if vmin is None:
        vmin = min(data_frame.min())
    if vmax is None:
        vmax = max(data_frame.max())
    biosemi_montage = mne.channels.make_standard_montage(kind='standard_1020', head_size=0.095)
    ch_names = list(data_frame.columns.values)
    n_channels = len(ch_names)
    info = mne.create_info(ch_names=ch_names, sfreq=500,
                           ch_types='eeg')
    fig, ax = plt.subplots(nrows=1, ncols=7, figsize=(20, 3))
    for i in range(len(keys_rhythms)):
        data = []
        for ch in ch_names:
            data.append([df[ch][keys_rhythms[i]]])
        evoked = mne.EvokedArray(data, info)
        evoked.set_montage(biosemi_montage)
        ax[i].set_title(keys_rhythms[i])
        mne.viz.plot_topomap(evoked.data[:, 0], evoked.info,
                                            axes=ax[i],
                                            vmin=vmin,
                                            vmax=vmax,
                                            names=ch_names,
                                            show_names=True,
                                            show=False,
                                            cmap='RdBu_r')
    plt.suptitle(name_of_signal + ' in %')
    plt.savefig(name_of_signal + ' in %')
    plt.show()


keys_rhythms = ['Theta1_Normed',
               'Theta2_Normed',
               'Alpha1_Normed',
               'Alpha2_Normed',
               'Alpha3_Normed',
               'Beta1_Normed',
               'Beta2_Normed']
titles = ('Eyes closed', 'Eyes open', 'n1', 'n2', 'Negative', 'Positive')
df = pd.read_csv('powers_positive.csv')
df.set_index('Freq_names', inplace=True, drop=True)
viz_head_eeg(df, keys_rhythms, titles[5])