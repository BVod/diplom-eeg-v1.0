from pyedflib import highlevel
import os
import pandas as pd
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors

#functin to cut signal
def signal_cut(F, f1, f2):
    n = len(F)
    for i in range(n):
        if F[i] >= f1:
            n1 = i
            break
    for i in range(n):
        if F[i] >= f2:
            n2 = i
            break
    num = [n1, n2]
    return num

def powerEEG(signals, Fs, signal_headers, freq_features, keys_rhythms, name_of_file, powers, num_person, mean_powers):
    N = len(signals[0])
    f = open('E:/Python/MyProjects/test1/resuts/' + name_of_file + '.txt', 'w')
    k = 0
    for i in range(len(signals)-1):
        N1 = 10*Fs                 #limits of signal
        N2 = len(signals[i])-10*Fs #length
        spectrum = np.fft.rfft(signals[i][N1:N2] - np.mean(signals[i][N1:N2])) #fft of signals
        power_spectrum = (2 * abs(spectrum) / N) ** 2 #square of each harmonic
        full_power = np.sum(power_spectrum) #full power of spectrum
        freq = np.fft.rfftfreq(N, 1. / Fs) #array of freq for fft
        ch_name = signal_headers[i]['label']
        ch_name = ch_name[3:len(ch_name)]
        f.write('<<< ' + ch_name + ' >>>\n')
        part_of_rhythm = np.zeros(len(keys_rhythms))
        df_array = []
        for j in range(len(keys_rhythms)):
            num = signal_cut(freq, freq_features[keys_rhythms[j]][0], freq_features[keys_rhythms[j]][1])
            #numbers of freq in spectrum
            rhythm_power = np.sum(power_spectrum[num[0]:num[1]]) #power of particular rhythm
            part_of_rhythm[j] = rhythm_power / full_power
            powers[k][num_person] = round(part_of_rhythm[j]*100, 2)
            mean_powers[k][num_person] = rhythm_power
            k = k + 1
            f.write(keys_rhythms[j] + ' = ' + str(round(part_of_rhythm[j]*100, 2)) + '%\n' )
        f.write('Total power = ' + str(round(np.sum(part_of_rhythm)*100, 2)) + '%\n\n')
    f.close()
    return powers, mean_powers



keys_rhythms = ['Theta1_Normed',
               'Theta2_Normed',
               'Alpha1_Normed',
               'Alpha2_Normed',
               'Alpha3_Normed',
               'Beta1_Normed',
               'Beta2_Normed']
freq_features = {
    'Theta1_Normed': [3.5, 5.8],
    'Theta2_Normed': [5.9, 7.4],
    'Alpha1_Normed': [7.5, 9.4],
    'Alpha2_Normed': [9.5, 10.7],
    'Alpha3_Normed': [10.8, 13.5],
    'Beta1_Normed': [13.6, 25],
    'Beta2_Normed': [25.1, 40]}

'''keys_rhythms = ['Theta1_Normed',
               'Theta2_Normed',
               'Alpha1_Normed',
               'Alpha2_Normed',
               'Alpha3_Normed',
               'Beta1_Normed',
               'Beta2_Normed',
                'Noise 0-3.5 Hz']
freq_features = {
    'Theta1_Normed': [3.5, 5.8],
    'Theta2_Normed': [5.9, 7.4],
    'Alpha1_Normed': [7.5, 9.4],
    'Alpha2_Normed': [9.5, 10.7],
    'Alpha3_Normed': [10.8, 13.5],
    'Beta1_Normed': [13.6, 25],
    'Beta2_Normed': [25.1, 40],
    'Noise 0-3.5 Hz': [0, 3.5]}'''

path = 'C:/Users/User/Downloads/Data-20200928T091501Z-001/Data' #main address
dirs = os.listdir(path) #list of content in Data folder
titles = ('eyes closed', 'eyes open', 'n1', 'n2', 'negative', 'positive')
Fs = 500
#person_number = 1 #the number of person Total:48
type_of_signal = 5 #choose type of signal
#i = person_number - 1 # index for person number

data_frame = pd.DataFrame()
data_frame['Freq_names'] = keys_rhythms
df_abs_powers = pd.DataFrame()
df_abs_powers['Freq_names'] = keys_rhythms
powers = np.zeros([19 * len(freq_features), len(dirs)]) #array for all powers
mean_powers = np.zeros([19 * len(freq_features), len(dirs)])
result_abs_power = np.zeros(19 * len(freq_features))
for i in range(len(dirs)):
#for i in [45]:
    #print(i)
    sig = os.listdir(path + '/' + dirs[i])
    num_sig = len(sig)  # number of signals for person
    for j in [type_of_signal]:    #type of signal
        name_of_file = sig[j][:-4]
        signals, signal_headers, header = highlevel.read_edf(path + '/' + dirs[i] + '/' + sig[j]) #read each edf file
        powers, mean_powers = powerEEG(signals, Fs, signal_headers, freq_features, keys_rhythms, name_of_file, powers, i, mean_powers)
f = open('E:/Python/MyProjects/test1/powers_' + titles[type_of_signal] + '.txt', 'w')
cnt = 0
for i in range(len(signals)-1):
    ch_name = signal_headers[i]['label']
    ch_name = ch_name[4:len(ch_name)]
    f.write('<<< ' + ch_name + ' >>>\n')
    df_array = []
    df_array_absolut = []
    for j in range(len(freq_features)):
        result_abs_power[cnt] = round(np.mean(mean_powers[cnt]), 4)
        f.write(keys_rhythms[j] + ' = ' + str(round(np.mean(powers[cnt]), 2)) + '%\n')
        df_array.append(round(np.mean(powers[cnt]), 2))
        df_array_absolut.append(round(np.mean(mean_powers[cnt]), 4))
        cnt = cnt + 1
    f.write('\n\n')
    data_frame[ch_name] = df_array
    df_abs_powers[ch_name] = df_array_absolut
data_frame.to_csv('powers_' + titles[type_of_signal] + '.csv', index=False)
df_abs_powers.to_csv('abs_powers_' + titles[type_of_signal] + '.csv', index=False)
f.close()



