import mne
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm


def viz_one_head(map=None, title=None, axes=None, plot_cbar=False, colorbar_title=None, vmin=None, vmax=None):
    """
    Function for visualizing one head using MNE package

    :param map: dict, Dictionary of channel activations. Keys are channel names and values are values to plot
    :param title: str, Text to use for the title
    :param axes: instance of Axis, The axis to plot to. If None, the current axis will be used.
    :param plot_cbar: bool, Whether to draw colorbar.
    :param colorbar_title: str, Text to use for the colorbar title
    :param vmin: int or float, value to use as lower bound in normalization
    :param vmax: int or float, value to use as upper bound in normalization
    :return: matplotlib.image.AxesImage, The interpolated data
    """
    biosemi_montage = mne.channels.make_standard_montage(kind='standard_1020', head_size=0.095)
    ch_names = list(map.keys())
    ch_vals = np.array(list(map.values()))
    ch_vals = ch_vals.reshape(ch_vals.shape[0], 1)
    info = mne.create_info(ch_names=ch_names, sfreq=500, ch_types='eeg')

    evoked = mne.EvokedArray(ch_vals, info)
    evoked.set_montage(biosemi_montage)

    vmin = vmin if vmin is not None else np.min(ch_vals)
    vmax = vmax if vmax is not None else np.max(ch_vals)

    if axes is None:
        fig, axes = plt.subplots()
        plt.suptitle(title, fontsize=20)
    else:
        axes.set_title(title, )

    img, contour = mne.viz.plot_topomap(evoked.data[:, 0], evoked.info,
                                        axes=axes,
                                        vmin=vmin,
                                        vmax=vmax,
                                        names=ch_names,
                                        show_names=True,
                                        show=False,
                                        cmap='RdBu_r')

    if plot_cbar:
        plt.colorbar(img, ax=axes, orientation='vertical', label=colorbar_title if colorbar_title is not None else "")

    # plt.tight_layout()

    return img


def test_1(ch_names):
    for j, ch in enumerate(ch_names):
        values1 = np.zeros(len(ch_names))
        values2 = values1 + 1

        values1[j] = 1
        values2[j] = 2

        data_dict1 = dict(zip(ch_names, values1))
        data_dict2 = dict(zip(ch_names, values2))

        vmin = min(np.minimum(values1, values2))
        vmax = max(np.maximum(values1, values2))

        fig, spaxs = plt.subplots(2, 1, facecolor='grey')
        fig.suptitle(ch)
        im = viz_one_head(map=data_dict1, axes=spaxs[0], vmin=vmin, vmax=vmax)
        im = viz_one_head(map=data_dict2, axes=spaxs[1], vmin=vmin, vmax=vmax)
        fig.subplots_adjust(right=0.8)
        cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        norm = cm.colors.Normalize(vmin=vmin, vmax=vmax)
        fig.colorbar(cm.ScalarMappable(norm=norm, cmap='RdBu_r'), cax=cbar_ax)
        plt.tight_layout()
    plt.show()


if __name__ == "__main__":

    ch_names = ['C3', 'C4', 'Cz', 'F3', 'F4', 'F7', 'F8', 'Fp1', 'Fp2', 'Fz', 'O1', 'O2', 'P3', 'P4', 'Pz', 'T3', 'T4',
                'T5', 'T6']

    keys_rhythms = ['Theta1_Normed',
                    'Theta2_Normed',
                    'Alpha1_Normed',
                    'Alpha2_Normed',
                    'Alpha3_Normed',
                    'Beta1_Normed',
                    'Beta2_Normed']
    titles = ('Eyes closed', 'Eyes open', 'n1', 'n2', 'Negative', 'Positive')
    df = pd.read_csv('powers_positive.csv')
    df.set_index('Freq_names', inplace=True, drop=True)

    vmin = min(df.min())
    vmax = max(df.max())

    # create figure
    fig, ax = plt.subplots(nrows=1, ncols=len(keys_rhythms), figsize=(25, 3))
    fig.suptitle('powers_positive.csv')

    # vizualize heads
    for i, rhythm in enumerate(keys_rhythms):

        data = {}
        for ch in ch_names:
            data.update({ch: df[ch][rhythm]})

        im = viz_one_head(map=data, axes=ax[i], vmin=vmin, vmax=vmax, title=rhythm)

    # add colorbar
    fig.subplots_adjust(right=0.8, top=0.9)
    cbar_ax = fig.add_axes([0.85, 0.1, 0.01, 0.8])
    norm = cm.colors.Normalize(vmin=vmin, vmax=vmax)
    fig.colorbar(cm.ScalarMappable(norm=norm, cmap='RdBu_r'), cax=cbar_ax, shrink=0.6)

    # test_1(ch_names)
    plt.show()
